-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2013 at 10:53 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mduuka`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `fullname` varchar(225) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(225) NOT NULL,
  `custid` int(225) NOT NULL AUTO_INCREMENT,
  `phone` text NOT NULL,
  `regdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`custid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`fullname`, `password`, `email`, `custid`, `phone`, `regdate`) VALUES
('mDuuka', 'c3dddc17e3daddc9c04f34c835d8ed54', 'mDuuka@gmail.com', 62, '0784191618', '2013-02-11 13:52:54'),
('gshhs', '70dfd7a5a7fa75ea30b6a6d47af356f9', 'gshshs', 64, '6549494', '2013-02-14 17:07:42'),
('Ahumuza Peter', 'b80f1ca05910dd1150225daef97caa53', 'hmz2peter@gmail.com', 66, '0785690848', '2013-02-16 10:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `delvid` int(25) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prodid` int(25) NOT NULL,
  `quantity` int(25) NOT NULL,
  `custmid` int(25) NOT NULL,
  PRIMARY KEY (`delvid`),
  KEY `custmid` (`custmid`),
  KEY `custmid_2` (`custmid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `orderid` int(25) NOT NULL AUTO_INCREMENT,
  `orderdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `custid` int(25) NOT NULL,
  `quantity` int(25) NOT NULL,
  `prodid` int(25) NOT NULL,
  PRIMARY KEY (`orderid`),
  KEY `custid` (`custid`),
  KEY `prodid` (`prodid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `prodid` int(25) NOT NULL AUTO_INCREMENT,
  `prodname` varchar(25) NOT NULL,
  `price` decimal(6,0) NOT NULL,
  `prodcategory` varchar(25) NOT NULL,
  `image` varchar(25) NOT NULL,
  `productdesc` text NOT NULL,
  PRIMARY KEY (`prodid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prodid`, `prodname`, `price`, `prodcategory`, `image`, `productdesc`) VALUES
(1, 'Chips', '10000', 'lunch', 'img 1', 'Chips'),
(2, 'Fish', '15000', 'lunch', 'img 2', 'Fish'),
(3, 'Pork', '25000', 'lunch', 'img 3', 'Pork'),
(4, 'Burger', '7000', 'lunch', 'img 4', 'Burger'),
(5, 'Pizza', '3500', 'lunch ', 'img 5', 'Pizza'),
(6, 'Mixed Grill', '25000', 'lunch ', 'img 6', 'Mixed Girll'),
(7, 'Pie', '5000', 'lunch', 'img 7', 'Pie'),
(8, 'Chicken wings', '10000', 'lunch ', 'img 8', 'Chicken wings'),
(9, 'Chicken drums', '12000', 'lunch', 'img 9', 'Chicken drums'),
(10, 'Pork ribs', '17000', 'lunch', 'img 10', 'Pork ribs');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`custmid`) REFERENCES `customer` (`custid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`custid`) REFERENCES `customer` (`custid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`prodid`) REFERENCES `product` (`prodid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
